Computational Lab: Data and Uncertainty 2
=============================

This is the tutorial material for Computational Lab 2 in the Data and
Uncertainty core course of the MPECDT MRes.

As for all of the computational labs in this course, you should
develop your solutions to these exercises in a git repository, hosted
on BitBucket. During the timetabled laboratory: 

- Work through the exercises on your laptop, by forking and checking out
  a copy of the mpecdt git repository. 
- If you would like help with issues in your code, either:

  - Paste the code into a gist, or
  - Push your code up to your forked git repository on BitBucket.
    Then, share the link to your gist or repository in the IRC channel

This Computational Lab is about the use of the Monte Carlo method to
calculate of statistics for random variables :math:`X`, of the form:
:math:`\mathbb{E}[f(X)]`, which satisfies

.. math::
   \theta := \mathbb{E}[f(X)] = \int f(x) \mu(dx),

where :math:`\mu` is the probability measure for 
:math:`X`.

There are a number of reasons for wanting to compute these integrals
in Planet Earth applications. The main situation is that we have a
model, mapping uncertain inputs :math:`x` to outputs :math:`g(x)`, and
we wish to compute statistics of the outputs, for example the mean

.. math::
   \mathbb{E}[g(X)] = \int g(x) \mu(dx),

provides an estimate of the quantity of interest, whilst the variance,

.. math::
   \mathbb{E}\left[(g(X)-\mathbb{E}[g(X)])^2\right],

provides a quantification of the uncertainty in this estimate.

These uncertainties may include:

- uncertainty in the initial conditions (e.g. the weather forecasting problem),
- stochastic terms in an SDE (e.g. a model for turbulence in an
  atmospheric dispersal model)
- random values of parameters (e.g. representing uncertainty about the
  friction coefficient underneath ice sheets)

For now, we assume that the model `f` itself is perfect, i.e. free
from errors.

In the most basic form of Monte Carlo method (named by Stan Ulam and
John von Neumann when predicting neutron fluxes at the Los Alamos
laboratory), we take :math:`N` independent, individually distributed
copies :math:`\{X_i\}_{i=1}^N` of the random variable :math:`X`, 
which we call "samples". Our estimator :math:`\hat{\theta}` is then 
defined as

.. math::
   \hat{\theta} = \frac{1}{N}\sum_{i=1}^N f(X_i).

(Note that this looks like a form of numerical quadrature, but with
equal weights, and random quadrature points).

If we assume that our model is perfect, then the estimator is unbiased, i.e.,

.. math::
   \mathbb{E}[\hat{\theta}] = \frac{1}{N}\sum_{i=1}^N\mathbb{E}[f(X_i)]
   =\frac{1}{N}\sum_{i=1}^N\theta = \theta.

By the central limit theorem, the rescaled error

.. math::
   Y = \frac{\hat{\theta}-\theta}{\sqrt{\sigma^2/N}},

(where :math:`\sigma` is the (unknown) variance of :math:`f(X)`) is
approximately :math:`N(0,1)` distributed. If we assume that :math:`Y` 
is exactly :math:`N(0,1)` distributed, then we can estimate the error in
terms of a confidence interval, e.g.

.. math::
   P\left(\hat{\theta}-2\sqrt{\frac{\sigma}{N}} < \theta < 
   \hat{\theta}+2\sqrt{\frac{\sigma}{N}} < \theta\right) = 0.954.

Exercise 1
--------

The file `mpecdt/src/montecarlo.py` currently computes the Monte
Carlo estimate of the variance of the normal distribution. The following
function takes two Python functions as inputs, one to evaluate `f` 
and one to provide samples from a random variable. ::

   def montecarlo(f, X, N):
      """
      Function to compute the Monte Carlo estimate of the expectation
      E[f(X)], with N samples
      
      inputs:
      f - a Python function that applies a chosen mathematical function to 
      each entry in a numpy array
      X - a Python function that takes N as input and returns
      independent individually distributed random samples from a chosen
      probability distribution
      N - the number of samples to use
      """

      theta = np.sum(f(X(N)))/N
      error_estimate = 0.0
      return theta, error_estimate

Below, we shall modify this code to return an error estimate, for now
this is just set to zero.

As provided, the executed code at the bottom calls `montecarlo` with
the following functions ::

  def square(x):
     """
     Function to compute the square of the input. If the input is 
     a numpy array, this returns a numpy array of all of the squared values.
     inputs:
     x - a numpy array of values

     outputs:
     y - a numpy array containing the square of all of the values
     """
     return x**2

  def normal(N):
     """
     Function to return a numpy array containing N samples from 
     a N(0,1) distribution.
     """
     return np.random.randn(N)

.. container:: tasks

   Execute the script, which will estimate the expectation of :math:`X^2`. 
   The correct value is 1. Modify the script by changing the value of 
   :math:`N`. Plot the error in a log-log plot against :math:`N`. 
   What is the slope? How does this relate to the 95.4\% confidence interval 
   error estimate above?

The 95.4\% confidence interval provides a useful characterisation of
the error provided that we know the variance of :math:`f(X)`. The
whole point of doing Monte Carlo is that we don't know the statistics
of :math:`f(X)`, so we must replace :math:`\sigma` with
:math:`\hat{\sigma}`, which is the sample variance,

.. math::
   \hat{\sigma} = \frac{1}{N-1}\sum_{i=1}\left(f(X_i)-\hat{\theta}\right)^2.

The factor of :math:`1/(N-1)` makes this estimator unbiased for normal
distributions. The following is then an estimate for the 95.4\%
confidence interval :math:`[\hat{\theta}-\epsilon,\hat{\theta}+\epsilon]`,

.. math::
   \hat{\epsilon} = 2\sqrt{\frac{\hat{\sigma}}{N}}.

.. container:: tasks

   Modify the `montecarlo` function so that the variable `error_estimate` 
   now returns the estimate above, which should be computed from the same
   random variables as are used to compute :math:`\hat{\theta}`. Plot this
   error estimate on a log-log graph and compare with the true error.

Exercise 2
----------

Since the Monte Carlo estimator is unbiased, it comes equipped with
its own approximate error estimator, described above. This means that
we can investigate the (approximate) error when we don't know the
answer. We'll try this out on the following test problem. Say that
our input `x` is a :math:`N(0,1)` variable as before, and that our "model"
is given by 

.. math::
   f(x) = \sqrt{x^2 + 1} + \sin(x).

.. container:: tasks

   Using the Monte Carlo method, estimate the mean and variance of
   :math:`f(x)` and obtain 95.4\% confidence intervals for both. What
   value of `N` is required for a 95.4\% confidence interval that is less
   than 10\% of the estimate values for both quantities?

Advanced topic
--------------

.. container:: tasks

   For an initial condition with distribution as given in Dynamics
   Computational Lab 2, estimate the mean and variance of the
   `x`-component of the Lorenz model at times t=1, 5, 10.
