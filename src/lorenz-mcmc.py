import numpy as np
import pylab
from scipy import integrate
import matplotlib.pyplot as plt

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x[:],y[:],z[:])
    pylab.show()
    
def plotmultiple3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    for i in range(0,len(x[1])):
       ax.plot(x[:,i],y[:,i],z[:,i])
    pylab.show()
    
def sample(sigma = 0.1):
    """
    Returns an initial condition [5.5, 8.3, 18.7] with some added noise N(0,sigma^2)
    """
    return np.array([5.5,8.3,18.7])+sigma*np.random.randn(3)
    
def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha, beta, rho - parameters

    Outputs:
    dxdt - the value of dx/dt
    """
    return [sigma*(x[1]-x[0]), x[0]*(rho-x[2])-x[1], x[0]*x[1]-beta*x[2]]
    
def getdata(y0, T = 100, Deltat = 0.05):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(10, 28, 8/3))
    return data
        
def plot_trajectories(N):
    """
    Plots the resulting Lorenz trajectories for N samples provided by the function "sample"
    
    Input:
    N - the number of smaples to take and resulting trajectories to plot
    """
    example_trajectories = np.zeros((20,3,N))
    for i in range(0,N):
        y0 = sample(sigma=0.1)
        data = getdata(y0, 1, 0.05)
        example_trajectories[:,:,i] = data
    plotmultiple3D(example_trajectories[:,0,:], example_trajectories[:,1,:], example_trajectories[:,2,:])

def observation(t_obs = 1, sigma = 0.5): 
    """
    chooses an initial condition y0 from the "sample" function and 'observes' the resulting
    Lorenz trajectory at time t_obs with observation error ~N(0,sigma^2).
    
    
    Inputs:
    t_obs - the time at which we observe the system 
    sigma - the s.d. of the observation error
    
    Outputs:
    out0 - y0 the initial condition
    out1 - yhat the observation plus error
    """
    y0 = sample(sigma=0.1)
    data = getdata(y0, t_obs, 0.05)
    return y0, data[-1,:] + sigma*np.random.randn(3)

def Phi(x, y, time = 1, sigma = 0.5):
    """
    Function to return Phi, assuming a normal distribution for 
    the observation noise, with mean 0 and variance sigma^2.
    inputs:
    x - a value of the model state x
    y - a value of the observed data
    time - time to run the lorenz system
    simga - observation noise s.d.
    """
    
    data = getdata(x, time, 0.05)
    forward_x = data[-1,:]
    return np.dot((forward_x-y),(forward_x-y))/(2*sigma**2)

def mcmc(prior_sampler,x0,yhat,N,beta, sigma_obs):
    """
    Function to implement the MCMC pCN algorithm.
    inputs:
    f - the function implementing the forward model
    prior_sampler - a function that generates samples from the prior
    x0 - the initial condition for the Markov chain
    yhat - the observed data
    N - the number of samples in the chain
    beta - the pCN parameter

    outputs:
    xvals - the array of sample values
    avals - the array of acceptance probabilities
    """
    
    # Ceate an array to store the x values, setting the first position to x0
    xvals = np.zeros((3,N+1))
    xvals[:,0] = x0  
    # Ceate an array to store the a values
    avals = np.zeros(N) 
    accept = np.zeros(N)  
    # Loop over the number of steps N
    for i in range(N):
        # draw w from the prior distribution
        w = prior_sampler()
        # set v (the prroposal) accordiing to the formula
        v = ((1-beta**2)**(0.5))*xvals[:,i] + beta*w 
        # Phi(x) calculates the norm of the diference between 
        # yhat and the forward function of x (over 2*sigma^2)
        phi_x = Phi(xvals[:,i], yhat, sigma = sigma_obs)
        phi_v = Phi(v, yhat,  sigma = sigma_obs)
        # The discriminant is calculated and the avals updated
        discriminant = np.exp(phi_x - phi_v)
        avals[i] = min(discriminant, 1)
        if i%100 == 0: 
            print "x_",i," = ",xvals[:,i]
            print "accepted so far: ",sum(accept)
            print ""
        # The proposal is accepted or rejected.
        if np.random.random()<avals[i]:
            xvals[:,i+1] = v 
            accept[i] = 1
        else: xvals[:,i+1] = xvals[:,i]
    return xvals,avals,accept
    
def ex2(sigma_obs = 1., x0 = np.array( [ 6,8,20]), T = 3000, beta=0.5):
    '''
    Runs the code involved in exercise 2. 
    Draws an initial condition y_0 from the prior and 'observes' the 
    Lorenz system at time t_obs to get y_hat then uses the mcmc function.
    
    Inputs:
    sigma_obs: the observation std
    x0:        the initial guess
    T:         number of steps in the mcmc
    beta:      the value of beta used in mcmc to select the proposal
    
    Output:
    Plots the cumulative a-values    
    '''
    y0, yhat = observation(t_obs = 1, sigma = sigma_obs)
    print "y_0 = ",y0
    print "Observation = ",yhat
    print "x_0 = ",x0
    xvals,avals,accept = mcmc(sample,x0,yhat,N = T,beta = beta, sigma_obs = sigma_obs)
    print "Final x value = ",xvals[:,-1]
    print "y_0 = ",y0
    print "Observation = ",yhat
    print "Total acceptances = ",sum(accept)
    avals = np.cumsum(avals)
    cumavavals = [avals[i]/(i+1) for i in range(len(avals))]
    accept = np.cumsum(accept)
    cumacceptancerate = [accept[i]/(i+1) for i in range(len(accept))]

   
   
    settledpoint=40
    endmean = np.mean(cumavavals[-20:-1])
    while np.var(cumavavals[(settledpoint-40):settledpoint])>0.0001 or abs(np.mean(cumavavals[(settledpoint-20):settledpoint]) - endmean)>0.01:
       settledpoint+=1
   
    print 'Settled point =  ', settledpoint
    
    pylab.plot(np.arange(1,T+1,1), cumavavals)
    #pylab.plot(np.arange(1,T+1,1), cumacceptancerate)
    pylab.plot([settledpoint,settledpoint],[cumavavals[settledpoint]-0.1,cumavavals[settledpoint]+0.1])
    pylab.plot([settledpoint-40,settledpoint-40],[cumavavals[settledpoint-40]-0.1,cumavavals[settledpoint-40]+0.1])
    
    pylab.ylabel("cumulative average a-values")
    pylab.xlabel("number of iterations of the algorithm")
    pylab.savefig('lorenz_mcmc_plot.png')
    pylab.show()
    

ex2(sigma_obs = 10., x0 = np.array( [0,0,0]), T = 1000, beta=0.833)
