import numpy as np
import pylab
from scipy import integrate
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """
    
    return np.array([sigma*(x[1]-x[0]),
                        x[0]*(rho-x[2])-x[1],
                        x[0]*x[1]-beta*x[2]])

def ensemble_plot(Ens,title=''):
    """
    Function to plot the locations of an ensemble of points.
    """

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(Ens[:,0],Ens[:,1],Ens[:,2],'.')    
    pylab.title(title)

def ensemble(M,T):
    """
    Function to integrate an ensemble of trajectories of the Lorenz system.
    Inputs:
    M - the number of ensemble members
    T - the time to integrate for
    """
    ens = 0.1*np.random.randn(M,3)

    for m in range(M):
        t = np.array([0.,T])
        data = integrate.odeint(vectorfield, 
                                y0=ens[m,:], t=t, args=(10.0,28.,8./3.),
                                mxstep=100000)
        #print data

        ens[m,:] = data[1,:]

    return ens

def f_1(d):
    """
    Function to apply ergodic average to.
    inputs:
    X - array, first dimension time, second dimension component
    outputs:
    f: - 1d- array of f values
    """

    return d[:,0]**2 + d[:,1]**2 + d[:,2]**2
    
def f_2(d):
	return d[:,0]**3 + d[:,1]**2 + d[:,2]
	
def f_3(d):
	return d[:,0]**2 + 6*d[:,1] + 3*d[:,2]**2
	
def f_4(d):
	return d[:,0] + 0.5*d[:,1]**4 + 2*d[:,2]**2
	
def f_5(d):
	return d[:,0]**5 + d[:,1]**4 + 2*d[:,2]**3
	
def f_6(d):
	return 3*d[:,0] + 4*d[:,1] + 2*d[:,2]
	

def space_average(M = 10, T = 15, N = 50, f = f_1):
	# Here we want to calulate the spatial average N times, for each time 't'. 
	# Perameters M = size on ensemble, T = maximum time. 
	# The N averages are computed at three times: t=T/2, t=3T/4 and t=T,
	# to demonstrate the nature of the varience which increase with t.
	#
	# First we crate the empty array to store our averages. This is reused for each 't'.
	average = np.zeros(N, dtype=float)
	# We loop over the three different times:
	for t in np.array(np.linspace(T/2,T,3), dtype=int):
		print '********************   Time = ',t,'   ************************'
		# compute the averages and store them to the array:
		for i in range(0,N): average[i] = np.average(f(ensemble(M,t)))
		# print a few averages to give a flavour of the numbers:
		for i in range(1,min(N,4)): print 'average',i,' : ',average[i] ;
		# print the varience of all the values in the array 'average':
		print 'Varience of all ',N,' averages :', np.var(average)
		print ' '
	# state the conclusion to provide an interesting talking point and provide inspiration for futute investigations:
	print 'Clearly, the varience of the spatial averages increases with time,'
	print 'due to the chaotic nature of the system.'
	
def time_average(N_min = 100, N_max = 5000, N_step = 10, Deltat = 0.1, f = f_1):
	# Here we want to run the Lorenz system for time $N\Delta t$, apply a function 'f' to each $X(t_n)$ and then take the average of these. 
	# We want to do this for several values of N (that is, the values in np.arange(N_min, N_max+1, N_step), given by the parameters) and plot the results.
	#
	# First we create our array of N values, given by our perameters, and a zero array for our averages we're going to compute.
	print 'We are plotting the average values of a function applied to the points on the Lorenz attractor (with a random initial point), sampled at time-intervals of length ',Deltat,' for varying total times.'
	print ' '
	print 'The time the system is run for is given by the product N x',Deltat,'.'
	N_values = np.arange(N_min, N_max+1, N_step)
	average = np.zeros_like(N_values, dtype=float)
	# We want to do everything for each value of N, so we loop over the N_values array.
	for i in range(len(N_values)):
		# We create an array of times, using Deltat, and integrate the vectorfield with initial conditions taken from a Gaussian.
		t = np.arange(0.,N_values[i]*Deltat,Deltat)
		data = integrate.odeint(vectorfield, np.random.randn(3), t=t, args=(10.0,28.,8./3.))
		# We then apply our function 'f' to each data point (spread over time) and take the average, storing that in our array.
		average[i] = np.average(f(data))
	# Now we return the N_values and the averages, so we can plot them against each other.
	# Now we plot the averages against the N_values.
	plt.plot(N_values, average)
	plt.xlabel('$N$')
	plt.ylabel('time-average')
	plt.show()
	print 'Note that the time average seems to converge (roughly) exponentially' 
	print 'That the time averages converge at all is surprising, given that the initially conditions have been chosen randomly.'
	print 'Clearly, the time averages are independent of the initial conditions.'
	
def time_average2(N = 1000, M = 50, Deltat = 0.1, f = f_1):
	# Here we want to run the Lorenz system for time $N\Delta t$, apply a function 'f' to each $X(t_n)$ and then take the average of these. 
	# We want to do this several times (M times) with the same vale of N.
	print 'Now we are computing the time average ',M,' times, with N=',N,'each time.'
	# We create a zero array for our averages we're going to compute.
	average = np.zeros(M, dtype=float)
	# We want to do everything M times,
	for i in range(M):
		# We create an array of times, using Deltat, and integrate the vectorfield with initial conditions taken from a Gaussian.
		t = np.arange(0.,N*Deltat,Deltat)
		data = integrate.odeint(vectorfield, np.random.randn(3), t=t, args=(10.0,28.,8./3.))
		# We then apply our function 'f' to each data point (spread over time) and take the average, storing that in our array.
		average[i] = np.average(f(data))
	print 'Mean of the time averages: ', np.mean(average)
	print 'Varience of the time averages: ',np.var(average)
	print 'If we take large enough M and N we see that the varience is very small, despite our initially conditions having been chosen randomly.'
	
def perturbing_all_perameters(T = 200, Deltat = 0.01, epsilon = 1.5):
	from mpl_toolkits.mplot3d import Axes3D
	# We want to see how much different parameters depend on the initial conditions, 
	# We define the standard values for sigma, rho and beta. In turn we will multiply each perameter by epsilon.
	sigma = 10
	rho = 28
	beta = 8/3
	print 'We are going to multiply each perameter by a factor of ',epsilon,' in turn'
	# We choose some random initial condition y_0,
	y_0 = np.random.randn(3)
	# then we intergrate the system for each case:
	t = np.arange(0.,T,Deltat)
	data = integrate.odeint(vectorfield, y_0, t=t, args=(sigma, rho, beta))
	data_sigma = integrate.odeint(vectorfield, y_0, t=t, args=(sigma*epsilon, rho, beta))
	data_rho = integrate.odeint(vectorfield, y_0, t=t, args=(sigma, rho*epsilon, beta))
	data_beta = integrate.odeint(vectorfield, y_0, t=t, args=(sigma, rho, beta*epsilon))
	
	# And plot our data
	fig = plt.figure()
	ax0 = fig.add_subplot(2, 2, 1, projection='3d')
	ax1 = fig.add_subplot(2, 2, 2, projection='3d')
	ax2 = fig.add_subplot(2, 2, 3, projection='3d')
	ax3 = fig.add_subplot(2, 2, 4, projection='3d')
	
	ax0.plot(data[:,0],data[:,1],data[:,2])
	ax1.plot(data_sigma[:,0],data_sigma[:,1],data_sigma[:,2])
	ax2.plot(data_rho[:,0],data_rho[:,1],data_rho[:,2])
	ax3.plot(data_beta[:,0],data_beta[:,1],data_beta[:,2])
	
	ax0.set_title('Standard parameters')
	ax1.set_title('Sigma perturbed')
	ax2.set_title('Rho perturbed')
	ax3.set_title('Beta perturbed')
	
	pylab.show()
	
	print 'It seems that varying Beta has the largest effect on the dynamics.'

	
def pertubing_rho(rho_0 = 28, Delta_rho = 0.1, N = 1000, Deltat = 0.1, steps = 100, f = f_1):
	# Here we want to run the Lorenz system for time $N\Delta t$, apply a function 'f' to each $X(t_n)$ and then take the average of these.
	# We want to do this with different values of the perameter rho in the system.
	rho = np.linspace(rho_0-Delta_rho, rho_0+Delta_rho, steps)
	averages = np.zeros_like(rho, dtype = float)
	partials = np.zeros_like(rho, dtype = float)
	y_0 = np.random.randn(3) 
	# We create an array of times, using Deltat,
	t = np.arange(0.,N*Deltat,Deltat)
	standard_data = integrate.odeint(vectorfield, y_0, t=t, args=(10.0,rho_0,8./3.))
	standard_average = np.average(f(standard_data))
	for i in range(len(rho)):
		# and integrate the vectorfield with initial condition y_0 taken from a Gaussian.
		data = integrate.odeint(vectorfield, y_0, t=t, args=(10.0,rho[i],8./3.))
		# We then apply our function 'f' to each data point (spread over time) and store the average.
		averages[i] = np.average(f(data))
		# We also calculate the partial derivative approximations, we don't want to do this too close to rho_0 though because we may divide by zero.
		if i<=0.4*steps or i>=0.6*steps:
			partials[i] = (averages[i] - standard_average)/(rho[i]-rho_0)
		else:
			partials[i] = partials[0]
	
	# Now we plot the averages and the partial derivative approximations
	a, axarr = plt.subplots(2, sharex=True)
	axarr[0].plot(rho, averages)
	axarr[0].set_ylabel('Time averages')
	axarr[1].plot(rho, partials)
	axarr[1].set_ylabel('Partial derivative')
	axarr[1].set_xlabel('rho')
	
	plt.show()
	print 'We can quite clearly see that the time averages are linear in rho.'
	
def pertubing_beta(beta_0 = 8./3., Delta_beta = 0.1, N = 1000, Deltat = 0.1, steps = 100, f = f_1):
	# Here we want to run the Lorenz system for time $N\Delta t$, apply a function 'f' to each $X(t_n)$ and then take the average of these.
	# We want to do this with different values of the perameter beta in the system.
	beta = np.linspace(beta_0-Delta_beta, beta_0+Delta_beta, steps)
	averages = np.zeros_like(beta, dtype = float)
	partials = np.zeros_like(beta, dtype = float)
	y_0 = np.random.randn(3) 
	# We create an array of times, using Deltat,
	t = np.arange(0.,N*Deltat,Deltat)
	standard_data = integrate.odeint(vectorfield, y_0, t=t, args=(10.0,28.0,beta_0))
	standard_average = np.average(f(standard_data))
	for i in range(len(beta)):
		# and integrate the vectorfield with initial condition y_0 taken from a Gaussian.
		data = integrate.odeint(vectorfield, y_0, t=t, args=(10.0,28.0,beta[i]))
		# We then apply our function 'f' to each data point (spread over time) and store the average.
		averages[i] = np.average(f(data))
		# We also calculate the partial derivative approximations, we don't want to do this too close to beta_0 though because we may divide by zero.
		if i<0.4*steps or i>=0.6*steps:
			partials[i] = (averages[i] - standard_average)/(beta[i]-beta_0)
		else:
			partials[i] = partials[0]
	
	# Now we plot the averages and the partial derivative approximations
	a, axarr = plt.subplots(2, sharex=True)
	axarr[0].plot(beta, averages)
	axarr[0].set_ylabel('Time averages')
	axarr[1].plot(beta, partials)
	axarr[1].set_ylabel('Partial derivative')
	axarr[1].set_xlabel('beta')
	
	plt.show()
	print 'It is harder to see that the time averages are linear in beta.'
	
def pertubing_sigma(sigma_0 = 10.0, Delta_sigma = 0.1, N = 1000, Deltat = 0.1, steps = 100, f = f_1):
	# Here we want to run the Lorenz system for time $N\Delta t$, apply a function 'f' to each $X(t_n)$ and then take the average of these.
	# We want to do this with different values of the perameter sigma in the system.
	sigma = np.linspace(sigma_0-Delta_sigma, sigma_0+Delta_sigma, steps)
	averages = np.zeros_like(sigma, dtype = float)
	partials = np.zeros_like(sigma, dtype = float)
	y_0 = np.random.randn(3) 
	# We create an array of times, using Deltat,
	t = np.arange(0.,N*Deltat,Deltat)
	standard_data = integrate.odeint(vectorfield, y_0, t=t, args=(sigma_0,28.0,8./3.))
	standard_average = np.average(f(standard_data))
	for i in range(len(sigma)):
		# and integrate the vectorfield with initial condition y_0 taken from a Gaussian.
		data = integrate.odeint(vectorfield, y_0, t=t, args=(sigma[i],28.0,8./3.))
		# We then apply our function 'f' to each data point (spread over time) and store the average.
		averages[i] = np.average(f(data))
		# We also calculate the partial derivative approximations, we don't want to do this too close to sigma_0 though because we may divide by zero.
		if i<0.4*steps or i>=0.6*steps:
			partials[i] = (averages[i] - standard_average)/(sigma[i]-sigma_0)
		else:
			partials[i] = partials[0]
	
	# Now we plot the averages and the partial derivative approximations
	a, axarr = plt.subplots(2, sharex=True)
	axarr[0].plot(sigma, averages)
	axarr[0].set_ylabel('Time averages')
	axarr[1].plot(sigma, partials)
	axarr[1].set_ylabel('Partial derivative')
	axarr[1].set_xlabel('sigma')
	
	plt.show()
	print 'That the time averages are linear in sigma is not very obvious.'
		
		

def exercise1():
	for T in [0.00001, 0.5, 1, 5]:
		Ens = ensemble(1, T)
		ensemble_plot(Ens,'T={time}'.format(time = T))
		pylab.show()
	
def exercise2():
	# ***We are going to examine the space average by calling the 'space_average' function
	#space_average()
	
	# ***Here we want to examine the time average, 
	# ***A smaller N_step will give better resolution but will slow everything down.
	time_average(N_min = 100, N_max = 2000, N_step = 100, Deltat = 0.1)
	
	# ***Now we will look at some time average taken over the same N value
	time_average2(N = 1000, M = 50, Deltat = 0.1)
	
if __name__ == '__main__':
	# ***One must first uncomment the exercise one wishes to run
	#exercise1()
	exercise2()
	#
	# ***Exercise 3 *** please read the comments and uncomment the part you wish to run
	#
	# ***First we want to see the effects of perturbing each of the Lorenz system perameters from the standard (10,28,8/3) values we've been using.
	#
	#perturbing_all_perameters(T = 200, Deltat = 0.01, epsilon = 5)
	#
	# ***Next we will investigate changes just to rho. We are considering values for rho in the interval (rho_0 - Delta_rho, rho_0 + Delta_rho). 
	# ***Increasing the perameter 'steps' in the call of the function will increase, the resolution of the plot (we take more rho values in the interval).
	# ***We can also experiment using different test functions f_2, f_3, f_4, f_5 and f_6, or a lambda function if you choose.
	# ***The time-averages / rho relationship seems more obviously linear with the lower order functions.
	#
	#pertubing_rho(rho_0 = 28, Delta_rho = 1, steps = 200, f = f_1)
	#
	# ***We can do the same thing for beta and sigma.
	# ***The linear relationship is not so pronounced.
	# 
	#pertubing_beta(beta_0 = 8./3., Delta_beta = 1, steps = 100, f = f_1)
	#pertubing_sigma(sigma_0 = 10.0, Delta_sigma = 1, steps = 100, f = f_1)
	#
	print 'If nothing has happened, this is because one must first uncomment one of the exercises at the bottom of the code'
	
	
	
	
	
	
