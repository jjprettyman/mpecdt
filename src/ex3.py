from scipy import integrate
import numpy as np
import matplotlib.pyplot as plt
import pylab
import math

def plot3D(x,y,z):
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()
    
def vectorfield(x, t, sigma, rho, beta):
    return [sigma*(x[1]-x[0]), x[0]*(rho-x[2])-x[1], x[0]*x[1]-beta*x[2]]

def getdata(y0,T,Deltat):
	t = np.arange(0.,T,Deltat)
	data = integrate.odeint(vectorfield, y0, t=t, args=(10, 28, 8/3))
	return data, t

def linear_lorenz_vectorfield(y,t, sigma, rho, beta):
    x = y[:3]
    dx = y[3:]
    xdot = [sigma*(x[1]-x[0]), x[0]*(rho-x[2])-x[1], x[0]*x[1]-beta*x[2]]
    dxdot = [sigma*(dx[1]-dx[0]), dx[0]*(rho-x[2])-dx[1]+x[0], dx[0]*x[1] + dx[1]*x[0] - dx[2]*beta]
    return np.concatenate((xdot,dxdot))

def Phi(x,dx,T, Deltat):
    y0 = np.concatenate((x,dx))
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(linear_lorenz_vectorfield, y0, t=t, args=(10.,28.,8./3.))
    return data[:,:3], data[:,3:], t

def Magnitude(p):
	mag = np.zeros_like(p[:,0])
	for i in range(0,len(mag)-1):
		mag[i] = math.sqrt(p[i,0]**2 + p[i,1]**2 + p[i,2]**2)
	return mag

def logMagnitude(p):
	logmag = np.zeros_like(p[:,0])
	for i in range(0,len(logmag)-1):
		logmag[i] = math.log(math.sqrt(p[i,0]**2 + p[i,1]**2 + p[i,2]**2))
	return logmag


#mydat, mytime = getdata([-0.587, -0.563, 16.870], 100, 0.05)

myphidat, perturbations, myphitime =  Phi([-0.587, -0.563, 16.870], [1,1,1], 100, 0.05)

#magnitude = Magnitude(perturbations)
log_mag = logMagnitude(perturbations)

pylab.plot(myphitime, log_mag)
pylab.show()

plot3D(myphidat[:,0],myphidat[:,1],myphidat[:,2])
