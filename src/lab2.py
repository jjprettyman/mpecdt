from scipy import integrate
import numpy as np
import matplotlib.pyplot as plt
import pylab
import math

exec(open("./data.py").read())
exec(open("./lyapunov.py").read())

def logdifference_array(dat1, dat2, time):
	difference = pylab.zeros_like(time)
	for t in range(0, len(time)):
		summation=0
		for i in range(0,len(dat1[0,:])):
			summation+=(dat1[t,i]-dat2[t,i])**2
		difference[t]=math.log(math.sqrt(summation))
	return difference

mydat, mytime = getdata([-0.587, -0.563, 16.870],100.0,0.05)
#mydat2, mytime2 = getdata(perturb([-0.587, -0.563, 16.870],10),100.0,0.05)
pert=1.01
mydat2, mytime2 = getdata([-0.587*pert, -0.563*pert, 16.870*pert],100.0,0.05)

myphidat, myphitime =  Phi([-0.587, -0.563, 16.870],[1,1,1],2000)
pylab.plot(myphitime, myphidat[:,0])
pylab.plot(myphitime, myphidat[:,1])
pylab.plot(myphitime, myphidat[:,2])
#pred = predict_linear(mydat)

#print(RSME(mydat,pred))
#print(RSME(mydat[:,0],pred[:,0]))
#restriction=20
#pylab.plot(mytime[0:restriction],mydat[0:restriction,0])
#pylab.plot(mytime[0:restriction],pred[0:restriction,0])
#plot3D(pred[:,0],pred[:,1],pred[:,2])
#plot3D(mydat[:,0],mydat[:,1],mydat[:,2])

#pylab.plot(mytime, logdifference_array(mydat,mydat2,mytime))
pylab.show()
