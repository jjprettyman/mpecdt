#!/usr/bin/env python3
#from scipy import odeint
from math import *
from numpy import *
import scipy.integrate as si
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D



def deriv(y,t):  # return derivatives of the array y
    sigma = 10
    rho = 28
    beta = 8/3
    return array([ sigma*(y[1]-y[0]),y[0]*(rho-y[2])-y[1],y[0]*y[1]-beta*y[2]])

maxt=100.0
time= linspace(0.0,maxt,100*maxt)
yinit= array([-0.587,-0.563,16.870])# initial values
y =si.odeint(deriv,yinit,time)

fig=plt.figure()
axes=fig.add_subplot(111, projection='3d')
axes.plot(y[:,0],y[:,1],y[:,2])

#plt.plot(time,y[:,0])
plt.show()
