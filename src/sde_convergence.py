import numpy as np
import pylab

def a(x,t):
    return -1.0*x

def b(x,t):
    return 1.0

def euler_maruyama(x,a,b,dt,T,tdump):
    """
    Solve the stochastic differential equation

    dx = a(x,t)dt + b(x,t)dW

    using the Euler-Maruyama method

    x^{n+1} = x^n = a(x^n,t^n)dt + b(x^n,t^n)dW

    inputs:
    x - initial condition for x
    a - a function that takes in x,t and returns a(x,t)
    b - a function that takes in x,t and returns b(x,t)
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    Wvals - a numpy array containing the values of W at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    Wvals = [0]
    W = 0.
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += dt*a(x,t) + b(x,t)*dW

        W += dW
        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
            Wvals.append(W)
    return np.array(xvals), np.array(tvals), np.array(Wvals)
    
def norm(x, exact_x, p):
	a = (abs(x - exact_x))**p
	return sum(a)**(1/p)

def exercise1():
	x,t,W = euler_maruyama(0.0,a,b,dt=0.001,T=10.0,tdump=0.01)
	pylab.plot(t,x)
	pylab.show()
	
def exercise2(c = -1, e = 1, x_0 = 1.0):
	x,t,W = euler_maruyama(x_0,lambda x,t: c*x, lambda x,t: e*x, dt=0.001,T=10.0,tdump=0.01)
	exact_x = x_0*np.exp((c-0.5*e**2)*t+e*W)
	error = abs(exact_x - x)
	#pylab.plot(W,x)
	#pylab.plot(W,exact_x)
	pylab.plot(t,error)
	pylab.show()
	
def exercise3(c = 1, e = 1, x_0 = 1.0, N = 100):
	dt = np.linspace(0.0001,0.1,100)
	L1 = np.zeros_like(dt)
	for i in range(0, len(dt)):
		for j in  range(1,N):
			x,t,W = euler_maruyama(x_0,lambda x,t: c*x, lambda x,t: e*x, dt=dt[i],T=1.0,tdump=0.01)
			exact_x = x_0*np.exp((c-0.5*e**2)*t+e*W)
			error = abs(exact_x - x)
		L1[i] = norm(x, exact_x, 1)
	pylab.plot(dt,L1)
	pylab.show()
	
#exercise1()
#exercise2()
exercise3()
	
