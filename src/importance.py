import numpy as np
import pylab
import matplotlib.pyplot as plt

"""
File containing code relating to Computational Lab 3 of the Dynamics
MPECDT core course.
"""

def square(x): return x**2

def normal(N): return np.random.randn(N)

def pi_Y(N): 
	"""
	returns a length N array of values sampled from the \pi_{Y} distribution defined in the question.
	"""
	u = np.random.rand(N)
	return -0.1*np.log(1-u+np.exp(-10)*u)
	
def pi_Y_pdf(x):
	"""
	maps real numbers x to the pdf \pi_{Y}
	"""
	y = np.zeros(x.shape, dtype = float)
	for i in range(len(y)):
		if x[i]>=0 and x[i]<=1: y[i] = 10*np.exp(-10*x[i]) / ( 1 - np.exp(-10*x[i]) )
	return y

def normal_pdf(x): return np.exp(-x**2/2)

def uniform_pdf(x):
    
    y = np.zeros(x.shape)
    y[x>0] = 1
    y[x>1] = 0.0
    return y

def importance(f, Y, rho, rhoprime, N):
    """
    Function to compute the importance sampling estimate of the
    expectation E[f(X)], with N samples

    inputs:
    f - a Python function that evaluates a chosen mathematical function on
    each entry in a numpy array
    Y - a Python function that takes N as input and returns
    independent individually distributed random samples from a chosen
    probability distribution
    rho - a Python function that evaluates the PDF for the desired distribution
    on each entry in a numpy array
    rhoprime - a Python function that evaluates the PDF for the
    distribution for Y, on each entry in a numpy array
    N - the number of samples to use
    """
    
    Y_array = Y(N)
    rho_ratio = rho(Y_array) / rhoprime(Y_array)

    return sum( f(Y_array)*rho_ratio ) / sum( rho_ratio )
    
def exercise1():
	"""
	Uses importance sampling to estimate the mean of the uniform distribution
	"""
	# We create an array of N values to use, I've used powers of ten to make the log plot look nice.
	powers = np.linspace(1,4,300)
	N = 10**powers
	# We also create a zero array to store the errors
	error = np.zeros_like(N, dtype=float)
	# Loop over the values of N. In each case we find our estimate theta 
	# and store the absolute value of the error. (Assuming the analytic value of the mean is 0.5)
	for i in range(len(N)):
		theta = importance(lambda x: x,normal,uniform_pdf,normal_pdf,N[i])
		error[i] = abs(theta - 0.5)
		
	# Plot the abs error, and on a loglog plot aswell.
	a, axarr = plt.subplots(2)
	axarr[0].plot(N,error)
	axarr[0].set_ylabel('absolute error')
	axarr[1].loglog(N,error)
	axarr[1].set_ylabel('absolute error')
	axarr[1].set_xlabel('N')
	
	plt.show()
	
def exercise2(maxpower = 5, resolution = 300):
	if resolution<100: resolution=100
	# You really can't see what's going on with any lower resolution. 
	"""
	In this example, we illustrate importance sampling as a method for reducing 
	the variance of a Monte Carlo estimator.
	
	Uses importance sampling to estimate the expected value of e^{-10X}\cos(X) 
	where X has a uniform distribution on [0,1]
	
	inputs: 
	maxpower - the maximum value of N considered is 10^maxpower
	resolution - the number of values of N to consider, ranging from 10^1 to 10^maxpower logarithmically. 
	"""
	# Compute the value of the expectation (theta) analytically.
	analytictheta = 10.0/101.0 - ( ( 10.0*np.cos(1) - np.sin(1) ) / ( 101.0*np.exp(10) ) )
	# We create an array of N values to use, I've used powers of ten to make the log plot look nice.
	powers = np.linspace(1,maxpower,resolution)
	N = 10**powers
	# Create arrays to store the data
	error = np.zeros_like(N, dtype=float)
	error_var = np.zeros_like(N, dtype=float)
	# Loop over the N values
	for i in range(len(N)):
		# Find a theta estimate using Importance Sampling and store the error.
		theta = importance(lambda x: np.exp(-10.0*x)*np.cos(x),pi_Y,uniform_pdf,pi_Y_pdf,N[i])
		error[i] = (theta - analytictheta)
		# Store the varience in the last 20 errors. Hopefully this is going to decrease.
		if i>50: error_var[i] = np.var(error[i-50:i])
	
	print 'The varience in the errors decreses with increasing N.'
	print 'Using a maximum value for N of 10^5 shows this relationship quite clearly.'
	
	# Plot both the error and the varience.
	# Note that we are plotting the absolute values of the errors.
	a, axarr = plt.subplots(2)
	axarr[1].semilogx(N,error_var)
	axarr[1].set_ylabel('True error moving 20-point varience')
	axarr[0].loglog(N,abs(error))
	axarr[0].set_ylabel('absolute error')
	axarr[1].set_xlabel('N')
	
	plt.show()
	
	
	

if __name__ == '__main__':
	
	#exercise1()
	exercise2(maxpower = 5, resolution = 300)

	
