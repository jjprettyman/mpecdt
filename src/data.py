

"""
File containing code relating to Computational Lab 1 of the Dynamics
MPECDT core course.

You should fork this repository and add any extra functions and
classes to this module.
"""

def vectorfield(x, t, sigma, rho, beta):
    """
    Function to return the value of dx/dt, given x.
    
    Inputs:
    x - the value of x
    t - the value of t
    alpha - a parameter
    beta - another parameter

    Outputs:
    dxdt - the value of dx/dt
    """

    return [sigma*(x[1]-x[0]), x[0]*(rho-x[2])-x[1], x[0]*x[1]-beta*x[2]]

def perturb(array, pc):
	new_array = pylab.zeros_like(array)
	for i in range(0,len(new_array)):
		new_array[i]=(1+pc/100)*array[i]
	return new_array
	

def getdata(y0,T,Deltat):
    """
    Function to return simulated observational data from a dynamical
    system, for testing out forecasting tools with.

    Inputs:
    y0 - the initial condition of the system state
    T - The final time to simulate until
    Deltat - the time intervals to obtain observations from. Note
    that the numerical integration method is time-adaptive and
    chooses it's own timestep size, but will interpolate to obtain
    values at these intervals.
    """
    t = np.arange(0.,T,Deltat)
    data = integrate.odeint(vectorfield, y0, t=t, args=(10, 28, 8/3))
    return data, t

def predict_linear(dat):
    prediction = np.zeros_like(dat)
    prediction[0,:]=dat[0,:]
    prediction[1,:]=dat[1,:]
    
    for i in range(2, len(dat)):
        prediction[i,:] = 2*dat[i-1,:]-dat[i-2,:]
    
    return prediction  

def RSME(x,y):
   
    N=len(x)

    summation = 0
    for n in range(0,N):
        summation += np.linalg.norm(np.subtract(x,y))**2
    return math.sqrt(summation/N)

def plot3D(x,y,z):
    """
    Make a 3D plot of data.
    Inputs:
    x - Numpy array of x coordinate
    y - Numpy array of y coordinate
    z - Numpy array of z coordinate
    """
    
    from mpl_toolkits.mplot3d import Axes3D

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x,y,z)
    pylab.show()
            
#if __name__ == '__main__':
    #mydat, mytime = getdata([-0.587, -0.563, 16.870],100.0,0.05)
    #pred = predict_linear(mydat)

    #print(RSME(mydat,pred))
    #print(RSME(mydat[:,0],pred[:,0]))
    #restriction=20
    #pylab.plot(mytime[0:restriction],mydat[0:restriction,0])
    #pylab.plot(mytime[0:restriction],pred[0:restriction,0])
    #pylab.show()
    ##plot3D(pred[:,0],pred[:,1],pred[:,2])
    ##plot3D(mydat[:,0],mydat[:,1],mydat[:,2])
