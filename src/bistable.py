import pylab
import numpy as np
from scipy.integrate import quad
import random

def Vprime_bistable(x):
    return x**3-x

def brownian_dynamics(x,Vp,sigma,dt,T,tdump):
    """
    Solve the Brownian dynamics equation

    dx = -V(x)dt + sigma*dW

    using the Euler-Maruyama method

    x^{n+1} = x^n - V(x^n)dt + sigma*dW

    inputs:
    x - initial condition for x
    Vp - a function that returns V'(x) given x
    dt - the time stepsize
    T - the time limit for the simulation
    tdump - the time interval between storing values of x

    outputs:
    xvals - a numpy array containing the values of x at the chosen time points
    tvals - a numpy array containing the values of t at the same points
    """

    xvals = [x]
    t = 0.
    tvals = [0]
    dumpt = 0.
    while(t<T-0.5*dt):
        dW = dt**0.5*np.random.randn()
        x += -Vp(x)*dt + sigma*dW

        t += dt
        dumpt += dt

        if(dumpt>tdump-0.5*dt):
            dumpt -= tdump
            xvals.append(x)
            tvals.append(t)
    return np.array(xvals), np.array(tvals)



def ex1():
	'''
	Implements the Euler-Maruyama method for the brownian dynamics model.
	We find that sigma=0.5 gives us some lovely bistable dynamics
	sigma>0.5 and the system is very unstable. As sigma approaches 1
	 we jump out of the wells as soon as we enter them. As we take sigma to 0
	 we just stay in one of the wells for ages, boring. 
	Clearly, with sigma=0 nothing happens at all.
	'''
	dt = 0.1
	sigma =0.5
	T = 1000.0
	x,t = brownian_dynamics(0.0,Vprime_bistable,sigma,dt,T,dt)
	pylab.plot(t,x)
	pylab.show()
	
def ex2(sigma = 0.5, ens_number = 1000):
	'''
	Implements the Euler-Maruyama method for the brownian dynamics model.
	Plots a histogram showing how many ensemble members ended up in each range of x values
	 after time T. (normed to give an estimate of the probability of ending in each range)
	We expect to see clusters around -1 and 1, the stable points because it is likely that our 
	 ensemble members will end up there.
	Takes inputs: sigma - the value of sigma
	              ens_number - the size of the ensemble to use.
	'''
	# set the values of dt and T to feed into the brownian_dynamics function.
	dt = 0.1
	T = 500.0
	
	# initialise the x_matrix which will store the x output from brownian_dynamics.
	x_matrix = np.zeros((ens_number,int(T/dt)))
	# Run brownian_dynamics ens_number of times and store the x output each time
	for i in range(0,ens_number):
		x,t = brownian_dynamics(1.0,Vprime_bistable,sigma,dt,T,dt)
		x_matrix[i,:] = x[1:len(x_matrix[0,:])+1]
	
	# define the arrays x and rhohat to plot against each other over the histogram
	# we take x in the range -2, 2 because this is where things are interesting,
	# and we compute rhohat - the unique steady state solution.
	# For rhohat to be normed it is necessary to compute the one-over-the integral numerically.
	C = 1/quad(lambda x: np.exp(x**2*(1-x**2/2)/sigma**2),-10,10)[0]
	x = np.linspace(-2.0,2.0,1000)
	rhohat = C*np.exp(x**2*(1-x**2/2)/sigma**2)
		
	print 'Here we have some nice plots. The smooth curve you see is the steady state solution.'
	print 'Note that our initial x value is 1, so our ensemble members are distributed quite densely around this to start with.'
	print 'It seems to me as if the solution converges to the steady state solution after around time 50'
	# Now we plot the histogram at times 5, 10, 50 and T (the final entry).
	for i in [int(5/dt), int(10/dt), int(50/dt), int(T/dt)]:
		pylab.hist(x_matrix[:,i], 100, normed = True )
		pylab.plot(x,rhohat)
		pylab.title("Distribution of ensemble members after time {time}".format(time = int(i*dt)))
		pylab.show()
		
def plot_for_ex3(f, T):
	'''
	Returns the cumulative average of a function f of the x array output from brownian_dynamics
	sigma and dt are set as 0.5 and 0.1 resp. 
	Takes the function f as an input
	'''
	sigma = 0.5
	dt = 0.1
	x, t = brownian_dynamics(4*(random.random()-0.5),Vprime_bistable,sigma,dt,T,dt)
	cum_average = np.cumsum(f(x))/np.arange(1,len(x)+1)
	return cum_average

def ex3():
	'''
	Plots abs value of the estimate for E[f(X)] given by the output of plot_for_ex3() minus the realvalue found by integrating f(x)*rhohat(x).
	Plots these in turn for f:x\mapsto x and f:x\mapsto x^2.
	'''
	print 'the rate of convergence in the first plot looks more exponential than anything else, but you might need to run this exercise a few times to convice yourself.'
	realvalue = 0.0
	cum_average = plot_for_ex3(f = lambda x: x, T = 2000)
	pylab.semilogy(abs(cum_average-realvalue))
	pylab.xlabel('N')
	pylab.ylabel('abs(estimate - solution)')
	pylab.title("for $f:x\mapsto x$")
	pylab.show()
	
	print 'Again, in the second plot, it looks a bit exponential. You could probably agrue that it is polynomial.'
	realvalue = 0.852136152
	cum_average = plot_for_ex3(f = lambda x: x**2, T = 100)
	pylab.semilogy(abs(cum_average-realvalue))
	pylab.xlabel('N')
	pylab.ylabel('abs(estimate - solution)')
	pylab.title("for $f:x\mapsto x^2$")
	pylab.show()
	


if __name__ == '__main__':
	while 1==1:
		print 'Enter a number one to three'
		print 'For the exercise you want to see!'
		x=0
		while x not in [1,2,3]:
			x = input("may I suggest exercise 1? ")
		if x==1:
			ex1()
		elif x==2:
			ex2(sigma = 0.5, ens_number = 1000)
		else:
			ex3()
	
