import numpy as np
import pylab
import math
import matplotlib.pyplot as plt

def square(x):
   """
   Function to compute the square of the input. If the input is
   a numpy array, this returns a numpy array of all of the squared values.
   inputs:
   x - a numpy array of values

   outputs:
   y - a numpy array containing the square of all of the values
   """
   return x**2
   
def f(X):
	output = np.zeros_like(X)
	for i in range(len(output)):
		output[i] = math.sqrt(X[i]**2 + 1) + np.sin(X[i])
	return output

def normal(N):
   """
   Function to return a numpy array containing N samples from
   a N(0,1) distribution.
   """
   return np.random.randn(N)

def montecarlo(f, X, N):
   """
   Function to compute the Monte Carlo estimate of the expectation
   E[f(X)], with N samples

   inputs:
   f - a Python function that applies a chosen mathematical function to
   each entry in a numpy array
   X - a Python function that takes N as input and returns
   independent individually distributed random samples from a chosen
   probability distribution
   N - the number of samples to use
   """

   F_RV = f(X(N))
   theta = np.sum(F_RV)/N
   
   sd_estimate = np.sum((F_RV - theta)**2)/(N-1)
   error_estimate = 2*math.sqrt(sd_estimate/N)
   return theta, sd_estimate, error_estimate

def exercise_1():
	N_values = np.arange(1000,10001,100)

	Error = np.zeros_like(N_values, dtype=float)
	TrueError = np.zeros_like(N_values, dtype=float)
	
	for i in range(len(N_values)):
		theta, sigma, Error[i] = montecarlo(square, normal, N_values[i])
		TrueError[i] = abs(theta-1)
	plt.loglog(N_values, Error, 'r')
	plt.loglog(N_values, TrueError, 'b')
	plt.show()
	
def exercise_2():
	N=10
	theta, sigma, error = montecarlo(f, normal, N)
	while 2*error > 0.1*theta:
		N+=10
		theta, sigma, error = montecarlo(f, normal, N)
	print N

if __name__ == '__main__':  
	#exercise_1()
	exercise_2()
	
